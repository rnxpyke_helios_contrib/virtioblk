.global mem_barrier.memory_barrier
.type mem_barrier.memory_barrier,@function
mem_barrier.memory_barrier:
	mfence
	ret
