.POSIX:
.SUFFIXES:

TARGET=virtioblk
include /home/crux/src/ddevault/local/share/mercury/driver.mk

build:
	source ../local/bin/mercuryenv; make virtioblk
.PHONY: build

virtioblk: cmd/virtioblk manifest.o
	LDFLAGS=manifest.o \
		$(HARE) build $(HAREFLAGS) -o $@ $<

manifest.o: manifest.ini
	objcopy -S -Ibinary -Oelf64-x86-64 \
		--rename-section .data=.manifest,contents \
		$< $@

TESTINIT_SRC=\
	cmd/testinit/main.ha \
	cmd/testinit/bootstrap.ha \
	cmd/testinit/driver.ha \
	cmd/testinit/loader.ha
testinit: $(TESTINIT_SRC)
		$(HARE) build $(HAREFLAGS) -T+init -o $@ cmd/$@

bootstrap.tar: virtioblk
bootstrap.tar:
	tar -cvf bootstrap.tar $^

boot.iso: syslinux.cfg testinit virtioblk
	mkdir -p isodir
	cp syslinux.cfg isodir/syslinux.cfg
	cp $(SYSLINUX)/mboot.c32 isodir/mboot.c32
	cp $(SYSLINUX)/ldlinux.c32 isodir/ldlinux.c32
	cp $(SYSLINUX)/libcom32.c32 isodir/libcom32.c32
	cp $(SYSLINUX)/isolinux.bin isodir/isolinux.bin

	cp $(BOOTLOADER) isodir/boot.bin
	cp $(HELIOS) isodir/helios
	cp testinit isodir/testinit
	cp bootstrap.tar isodir/bootstrap.tar

	mkisofs -l -o $@ -b isolinux.bin -c boot.cat \
		-no-emul-boot -boot-load-size 4 -boot-info-table isodir
	isohybrid $@

drive.qcow2:
	qemu-img create -f qcow2  $@ 1M

test: boot.iso drive.qcow2
	$(QEMU) $(QEMUFLAGS) \
		-m 1G -no-reboot -no-shutdown \
		-drive file=boot.iso,format=raw \
		-drive file=drive.qcow2,format=qcow2,if=virtio \
		-serial stdio

.PHONY: test

clean:
	rm -f manifest.o
	rm -f virtioblk
	rm -f testinit

.PHONY: all clean virtioblk
