# Hello Mercury!

This is a simple VirtIO blockdevice driver built against the [Mercury][0]
driver environment for [Helios][1].

[0]: https://git.sr.ht/~sircmpwn/mercury
[1]: https://git.sr.ht/~sircmpwn/helios

## Usage

- Clone and build a working version of helios and mercury.
- Install merkury using `make install`
  (if you don't want to run as root, configure install scripts beforehand)
- To build, source `merkuryenv` and use `make`.
